<?php
$fname = $_POST['fname'];
$lname = $_POST['fname'];
$email = $_POST['email'];
$password = $_POST['password'];
$seatbought = $_POST['psw'];

$totalamount = $seatbought * 23;

if(!empty($fname) || !empty($lname) || !empty($email) || !empty($password) || !empty($seatbought)) {
    $host = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbname = "cookies";

    $conn = new mysqli($host, $dbUsername, $dbPassword, $dbname);

    if(mysqli_connect_error()){
        die('Connect Error('.mysqli_connect_errno().')'.mysqli_connect_error());
    } else {
        $SELECT = "SELECT clientEmail FROM cookies WHERE clientEmail = ? Limit 1 ";
        $INSERT = "INSERT INTO cookies (clientFname, clientLname, clientUsername, clientEmail, clientPassword, seatBought)
               VALUES (?,?,?,?,?,?)";

        $stmt = $conn->prepare($SELECT);
        $stmt-> bind_param("s", $email);
        $stmt-> execute();
        $stmt->bind_result($email);
        $stmt->store_result();
        $rnum = $stmt->num_rows;

        if($rnum == 0){
            $stmt->close();

            $stmt = $conn->prepare($INSERT);
            $stmt-> bind_param("iiiiiiiiii", $fname, $lname, $email, $password, $seatbought);
            $stmt->execute();
            echo "Transaction successful";
        }
    }

} else {
    echo "All fields are required";
    die();
}


<!DOCTYPE html>
<html lang="en">
<head>

    <title>Cookie's Reservation Ticket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src= " https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js "></script>
    <script src= " https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js "></script>
    <link rel="stylesheet" href= " https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css ">
    <link rel="stylesheet" type="text/css" href="app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <script src="app.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">cOOkies!</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#myPage">HOME</a></li>
                <li><a href="#band">ABOUT US</a></li>
                <li><a href="#tour">BUY A TICKET</a></li>
                <li><a href="#contact">CONTACT US</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">MORE
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Merchandise</a></li>
                        <li><a href="#">Extras</a></li>
                        <li><a href="#">Media</a></li>
                    </ul>
                </li>
                <li><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="home_pic.jpg" alt="01" width="1200" height="700">
        </div>

        <div class="item">
            <img src="home_pic2.jpg" alt="02" width="1200" height="700">
        </div>

        <div class="item">
            <img src="home_pic3.jpg" alt="03" width="1200" height="700">
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Container (The Band Section) -->
<div id="band1" class="container text-center">
    <h3>What Are you waiting for?</h3>
    <p><em>Buy a ticket Now!</em></p>
    <p>We have created a website that will help you reserve a seat and buy a ticket of a certain band. You can try and buy a ticket</p>
    <br>
</div>
<!-- Container (TOUR Section) -->
<div id="tour" class="bg-1">
    <div class="container">
        <h3 class="text-center">Reserve A Ticket Now!</h3>
        <p class="text-center"> You can also get the ticket on the booth after you reserve it here <br> or you can print the ticket!</p>
        <ul class="list-group">
            <li class="list-group-item">Day 1 - Paramore <span class="label label-danger"> Sold out!</span></li>
            <li class="list-group-item">Day 2 - Paramore  <span class="label label-danger">5 more remaining!</span></li>
            <li class="list-group-item">Day 3 - Paramore <span class="badge">3</span></li>
        </ul>

        <div class="row text-center">
            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="paris.jpg" alt="Paris" width="400" height="300">
                    <p><strong>Paris</strong></p>
                    <p>Friday 27 November 2015</p>
                    <button class="btn" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="newyork.jpg" alt="New York" width="400" height="300">
                    <p><strong>New York</strong></p>
                    <p>Saturday 28 November 2015</p>
                    <button class="btn" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="sanfran.jpg" alt="San Francisco" width="400" height="300">
                    <p><strong>San Francisco</strong></p>
                    <p>Sunday 29 November 2015</p>
                    <button class="btn" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4><span class="glyphicon glyphicon-lock"></span> Tickets</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="submit.php" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="psw"><span class="glyphicon glyphicon-shopping-cart"></span> Tickets, $23 per person</label>
                            <input type="number" class="form-control" id="psw" name="psw" placeholder="How many?" required>
                        </div>
                        <div class="form-group">
                            <label for="seats"><span class="glyphicon glyphicon-user"></span> Choose a seat</label>
                            <select name="seats" id="seats" required>
                                <option value="freeseating">Free Seating</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="fname"><span class="glyphicon glyphicon-user"></span> First Name</label>
                            <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter First Name" required>
                        </div>
                        <div class="form-group">
                            <label for="lname"><span class="glyphicon glyphicon-user"></span> Last Name</label>
                            <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter First Name" required>
                        </div>
                        <div class="form-group">
                            <label for="email"><span class="glyphicon glyphicon-user"></span> Email </label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="password"><span class="glyphicon glyphicon-user"></span> Last Name</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                        </div>

                        <button type="submit" class="btn btn-block">Pay
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                    <p>Need <a href="#">help?</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container (The Band Section) -->
<div id="band" class="container text-center">
    <h3>Cookie Members</h3>
    <p><em>We love music!</em></p>
    <p>We have created a website that will help a costumer to reserve a ticket of a certain band. You can try and click each picture to know more about the maker of this website.</p>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <p class="text-center"><strong>Chantall Arrio</strong></p><br>
            <a href="#demo" data-toggle="collapse">
                <img src="chantall.jpg" class="img-circle person" alt="Chantall" width="255" height="255">
            </a>
            <div id="demo" class="collapse">
                <p>Loves to Sing in inside and outside the bathroom</p>
                <p>Loves color Pink</p>
                <p>Likes to watch Series/Movies</p>
            </div>
        </div>
        <div class="col-sm-4">
            <p class="text-center"><strong>Bianca Gualdrapa</strong></p><br>
            <a href="#demo2" data-toggle="collapse">
                <img src="bianca.jpg" class="img-circle person" alt="Biancakes" width="255" height="255">
            </a>
            <div id="demo2" class="collapse">
                <p>Drummer</p>
                <p>Loves drummin'</p>
                <p>Member since 1998</p>
            </div>
        </div>
        <div class="col-sm-4">
            <p class="text-center"><strong>Joeven Andoy</strong></p><br>
            <a href="#demo3" data-toggle="collapse">
                <img src="bandmember.jpg" class="img-circle person" alt="Jovita" width="255" height="255">
            </a>
            <div id="demo3" class="collapse">
                <p>Bass player</p>
                <p>Loves math</p>
                <p>Member since 2005</p>
            </div>
        </div>
    </div>
</div>

<!-- Container (Contact Section) -->
<div id="contact" class="container">
    <h3 class="text-center">Contact Us</h3>
    <p class="text-center"><em>We love to hear your comments!</em></p>

    <div class="row">
        <div class="col-md-4">
            <p>Want Updates?.</p>
            <p><span class="glyphicon glyphicon-map-marker"></span>Philippines</p>
            <p><span class="glyphicon glyphicon-phone"></span>Phone: +63 0934093418</p>
            <p><span class="glyphicon glyphicon-envelope"></span>Email: cookie@ymail.com</p>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>

                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                </div>
            </div>
            <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
            <br>
            <div class="row">
                <div class="col-md-12 form-group">
                    <button class="btn pull-right" type="submit">Send</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <h3 class="text-center">From The Blog</h3>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Mike</a></li>
        <li><a data-toggle="tab" href="#menu1">Chandler</a></li>
        <li><a data-toggle="tab" href="#menu2">Peter</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h2>Mike Ross, Manager</h2>
            <p>Man, we've been on the road for some time now. Looking forward to lorem ipsum.</p>
        </div>
        <div id="menu1" class="tab-pane fade">
            <h2>Chandler Bing, Guitarist</h2>
            <p>Always a pleasure people! Hope you enjoyed it as much as I did. Could I BE.. any more pleased?</p>
        </div>
        <div id="menu2" class="tab-pane fade">
            <h2>Peter Griffin, Bass player</h2>
            <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
        </div>
    </div>
</div>


<!-- Footer -->
<footer class="text-center">
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Cookies Reservation Ticket</p>
</footer>

</body>
</html>
